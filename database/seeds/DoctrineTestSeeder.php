<?php

use Entity\ACL;
use Illuminate\Database\Seeder;
use Leebo\Acl\AclType;
use Entity\Group;
use Entity\PasswordReset;
use Entity\Registration;
use Entity\User;

class DoctrineTestSeeder extends Seeder
{

    /** @var  Faker\Generator */
    private $faker;

    private $players = [];

    private function purgeDatabase()
    {
        em::getConnection()->exec('DELETE FROM acl;');
        em::getConnection()->exec('DELETE FROM password_resets;');
        em::getConnection()->exec('DELETE FROM registrations;');
        em::getConnection()->exec('DELETE FROM user_settings;');
        em::getConnection()->exec('DELETE FROM groups;');
        em::getConnection()->exec('DELETE FROM users;');
    }

    private function createAcl(User $user, $name, $permission)
    {
        $acl = new ACL();
        $acl->setUser($user);
        $acl->setName($name);

        $acl->setPermission($permission);
        em::persist($acl);

        em::flush();
    }

    /**
     * Create admin related permissions
     * @param User $admin
     */
    private function createAdminAclList(User $admin)
    {
        $this->createAcl($admin, AclType::ADMIN_ACCESS, AclType::READ_ACCESS);
        $this->createAcl($admin, AclType::ADMIN, AclType::READ_ACCESS);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create faker
        $this->faker = Faker\Factory::create();
        
        $this->purgeDatabase();

        // Create the groups tables for administrator and user.
        $adminGroup = new Group();
        $adminGroup->setName('administrator');
        $adminGroup->setActive(true);
        em::persist($adminGroup);

        $userGroup = new Group();
        $userGroup->setName('player');
        $userGroup->setActive(true);
        em::persist($userGroup);

        //--------------------------
        // Create Users
        //--------------------------
        $admin = new User([
            'username' => 'admin',
            'email' => 'admin@nodomain.com',
            'password' => hash('sha512', '1234'),
            'firstName' => 'Gordon',
            'lastName' => 'Freeman',
            'active' => true,
            'status' => User::STATUS_VERIFIED
        ]);
        $admin->setGroup($adminGroup);
        em::persist($admin);
        em::flush();

        // Give admin permissions
        $this->createAdminAclList($admin);

        // Create one known user and create a PasswordReset for him/her.
        $plainUser = new User([
            'username' => 'plain_user',
            'email' => 'plainuser@none.com',
            'password' => hash('sha512', '1234'),
            'firstName' => 'Plain',
            'lastName' => 'User',
            'active' => true,
            'status' => User::STATUS_VERIFIED
        ]);
        $plainUser->setGroup($userGroup);
        em::persist($plainUser);
        em::flush();

        $reset = new PasswordReset('1234');
        $reset->setExpiry(new \DateTime('+30 days'));
        $reset->setUserId($plainUser->getId());
        em::persist($reset);

        $faker = $this->faker;

        // Rank and file users
        for($levelNum=0; $levelNum < 10; $levelNum++) {
            $user = new User([
                'username' => $levelNum . $this->faker->userName,
                'email' => $this->faker->email,
                'password' => hash('sha512', '1234'),
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'active' => true,
                'status' => User::STATUS_VERIFIED
            ]);

            $user->setGroup($userGroup);
            em::persist($user);

            em::persist($plainUser);

            $this->players[] = $user;
        }

        // Create a user with registration
        $regUser = new User([
            'username' => 'reg_user',
            'email' => 'reg_user@reg_user.com',
            'firstName' => 'test',
            'lastName' => 'test',
            'status' => User::STATUS_REGISTERED_NOT_VERIFIED
        ]);
        $regUser->setGroup($userGroup);
        em::persist($regUser);

        $reg = new Registration();
        $reg->setToken('1234');
        $reg->setExpiry(new \DateTime('+30 days'));
        $reg->setUser($regUser);
        em::persist($reg);
        em::flush();

    }
}
