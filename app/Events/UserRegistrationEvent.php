<?php

namespace Tourney\Events;

use Tourney\Entity\User;
use Tourney\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserRegistrationEvent extends Event
{
    use SerializesModels;

    /** @var  User */
    public $user;

    /** @var  string Token generated from registration process. */
    public $token;

    /**
     * Create a new event instance.
     * @param User $user
     */
    public function __construct(
        User $user,
        $token
    ) {
        parent::__construct();
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
