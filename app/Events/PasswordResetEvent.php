<?php

namespace Tourney\Events;

use Tourney\Entity\User;
use Tourney\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PasswordResetEvent extends Event
{
    use SerializesModels;

    /** @var  User */
    public $user;

    /**
     * Create a new event instance.
     * @param User $user
     */
    public function __construct(
        User $user
    ) {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
