<?php

namespace Tourney\Events;

use Tourney\Entity\User;
use Tourney\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserLoginEvent extends Event
{
    use SerializesModels;

    /** @var  User */
    public $user;

    /** @var  boolean */
    public $loggingIn;

    /**
     * Create a new event instance.
     * @param User $user
     * @param $loggingIn
     */
    public function __construct(
        User $user,
        $loggingIn
    ) {
        parent::__construct();
        $this->loggingIn = $loggingIn;
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
