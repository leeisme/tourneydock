<?php

namespace Tourney\Events;

use Tourney\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LeagueMembershipDecisionEvent extends Event
{
    use SerializesModels;

    /** @var string  */
    public $msg = '';

    /** @var bool Indicates if request was accepted. */
    public $accepted = false;

    /**
     * Create a new event instance.
     * @param $accepted
     * @param string $msg
     */
    public function __construct(
        $accepted,
        $msg = ''
    ) {
        parent::__construct();
        $this->accepted = $accepted;
        $this->msg = $msg;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
