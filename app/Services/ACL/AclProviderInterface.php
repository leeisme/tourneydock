<?php

namespace Leebo\Acl;

interface AclProviderInterface
{

    /**
     * @param integer $userId
     * @param string $aclName
     * @param integer $permission
     * @return void
     */
    public function createAcl($userId, $aclName, $permission);

    /**
     * @param integer $userId
     * @param string $aclName
     * @return boolean
     */
    public function removeAcl($userId, $aclName);

    /**
     * @param integer $userId
     * @param string $aclName
     * @return Acl
     */
    public function getAcl($userId, $aclName);

    /**
     * @param integer $userId
     * @param string $aclName
     * @return bool
     */
    public function canAccess($userId, $aclName);

    /**
     * @param $userId
     * @param $aclName
     * @return mixed
     */
    public function forbidden($userId, $aclName);

    /**
     * @param integer $userId
     * @param string $aclName
     * @return bool
     */
    public function canWrite($userId, $aclName);

    /**
     * @param integer $userId
     * @param string $aclName
     * @return int
     */
    public function value($userId, $aclName);

}