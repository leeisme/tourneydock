<?php

namespace Leebo\Acl;

class AclType
{
    const NO_ACCESS = 0;
    const READ_ACCESS = 1;
    const READ_WRITE_ACCESS = 2;

    const ADMIN = 'admin';
    const ADMIN_ACCESS = 'admin_access';
    const LEAGUE_ADMIN = 'league_admin';
    const PLAYER_LOGIN = 'player_login';
}