<?php

namespace Leebo\Acl;

use Doctrine\ORM\EntityManager;
use Entity\Acl;
use Entity\User;
use Tourney\Exceptions\ServiceException;

class DoctrineAclProvider implements AclProviderInterface
{
    /** @var  EntityManager */
    private $entityManager;

    function __construct(

    ) {
        $this->entityManager = \em::createQuery()->getEntityManager();
    }

    /**
     * @param $userId
     * @param $aclName
     * @param bool $exceptOnNotFound
     * @return Acl
     * @throws ServiceException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function findAcl($userId, $aclName, $exceptOnNotFound = false)
    {
        $user = $this->entityManager->find('Entity\\User', $userId);
        if (!$user instanceof User) {
            throw new ServiceException("User not found", 404);
        }

        $qry = $this->entityManager->createQuery("
            SELECT
                acl
            FROM
              Entity\\Acl acl
            WHERE
              acl.user = :user
              AND
              acl.name = :name
        ");

        $qry->execute([
            'user' => $user,
            'name' => $aclName
        ]);

        $existing = $qry->getOneOrNullResult();

        if (!$existing instanceof Acl && $exceptOnNotFound) {
            throw new ServiceException("Permission not found: $aclName", 404);
        }

        return $existing;
    }

    /**
     * @param integer $userId
     * @param string $aclName
     * @param integer $permission
     * @throws ServiceException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function createAcl($userId, $aclName, $permission)
    {
        $user = $this->entityManager->find('Entity\\User', $userId);
        if (!$user instanceof User) {
            throw new ServiceException("User not found", 404);
        }

        $existing = $this->findAcl($userId, $aclName);

        if ($existing instanceof Acl) {
            throw new ServiceException("Acl permission {$aclName} already exists for user", 403);
        }

        $acl = new Acl();
        $acl->setUser($user);
        $acl->setName($aclName);
        $acl->setPermission($permission);
        $this->entityManager->persist($acl);
        $this->entityManager->flush();

    }

    /**
     * @param int $userId
     * @param string $aclName
     * @return bool|void
     * @throws ServiceException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function removeAcl($userId, $aclName)
    {
        $user = $this->entityManager->find('Entity\\User', $userId);
        if (!$user instanceof User) {
            throw new ServiceException("User not found", 404);
        }

        $existing = $this->findAcl($userId, $aclName, true);

        $this->entityManager->remove($existing);
        $this->entityManager->flush();
    }

    /**
     * @param $userId
     * @param $aclName
     * @return Acl
     */
    public function getAcl($userId, $aclName)
    {
        return $this->findAcl($userId, $aclName);
    }

    /**
     * @param integer $userId
     * @param string $aclName
     * @return bool
     */
    public function canAccess($userId, $aclName)
    {
        return $this->value($userId, $aclName) >= 1;
    }

    /**
     * @param integer $userId
     * @param string $aclName
     * @return bool
     */
    public function canWrite($userId, $aclName)
    {
        return $this->value($userId, $aclName) >= 2;
    }

    /**
     * @param integer $userId
     * @param string $aclName
     * @return int
     */
    public function value($userId, $aclName)
    {
        $acl = $this->findAcl($userId, $aclName, true);

        return $acl->getPermission();
    }

    /**
     * @param $userId
     * @param $aclName
     * @return mixed
     */
    public function forbidden($userId, $aclName)
    {
        return $this->value($userId, $aclName) < 1;
    }
}