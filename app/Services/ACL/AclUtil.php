<?php

namespace Leebo\Acl;

use EM;
use Fudave\Entity\ACL;
use Fudave\Entity\User;
use Fudave\Repositories\AclRepoInterface;

class AclUtil
{

    /** @var  AclProviderInterface */
    private $aclProvider;

    /** @var  AclRepoInterface */
    private $aclRepo;

    function __construct(
        AclProviderInterface $aclProviderInterface,
        AclRepoInterface $aclRepoInterface
    ) {
        $this->aclProvider = $aclProviderInterface;
        $this->aclRepo = $aclRepoInterface;
    }

    private function createAcl(User $user, $name, $permission)
    {
        $acl = new ACL();
        $acl->setUser($user);
        $acl->setName($name);
        $acl->setPermission($permission);
        em::persist($acl);

        em::flush();
    }

    public function addUserAcl(User $user)
    {
        $this->createAcl($user, AclType::ADMIN_ACCESS, AclType::READ_ACCESS);

    }

    public function removeUserAcl(User $user, $name)
    {
        $acl = $this->aclRepo->getBy([
            'user_id' => $user->getId(),
            'name' => $name
        ]);

        if ($acl instanceof ACL) {
            em::remove($acl);
            em::flush($acl);
        }
    }
}