<?php

namespace Tourney\Services;

use Tourney\Exceptions\ServiceException;

class AbstractService
{
    protected function throwException($msg, $code)
    {
        throw new ServiceException($msg, $code);
    }
}