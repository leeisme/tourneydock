<?php

namespace Tourney\Services;

use Auth;
use EM;
use Event;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Support\Facades\Input;
use Log;
use Session;
use Entity\PasswordReset;
use Entity\Registration;
use Entity\User;
use Tourney\Events\PasswordResetEvent;
use Tourney\Events\UserConfirmationEvent;
use Tourney\Events\UserLoginEvent;
use Tourney\Events\UserRegistrationEvent;
use Tourney\Exceptions\ServiceException;
use Repositories\GroupRepoInterface;
use Repositories\LeagueRepoInterface;
use Repositories\PasswordResetRepoInterface;
use Repositories\RegistrationRepoInterface;
use Repositories\UserRepoInterface;

class IdentityService extends AbstractService
{
    /** @var  UserRepoInterface */
    private $userRepo;

    /** @var  PasswordResetRepoInterface */
    private $passwordResetRepo;

    /** @var  GroupRepoInterface */
    private $groupRepo;

    /** @var  LeagueRepoInterface */
    private $leagueRepo;

    /** @var  RegistrationRepoInterface */
    private $regRepo;

    function __construct(
        UserRepoInterface $userRepoInterface,
        PasswordResetRepoInterface $passwordResetRepoInterface,
        GroupRepoInterface $groupRepoInterface,
        LeagueRepoInterface $leagueRepoInterface,
        RegistrationRepoInterface $registrationRepoInterface
    ) {
        $this->userRepo = $userRepoInterface;
        $this->passwordResetRepo = $passwordResetRepoInterface;
        $this->groupRepo = $groupRepoInterface;
        $this->leagueRepo = $leagueRepoInterface;
        $this->regRepo  = $registrationRepoInterface;
    }

    /**
     * Test validity of user (active, suspended, etc) and throw appropriate exception
     * @param User $user
     */
    public function checkUserValid(User $user)
    {
        if (!$user->getActive()) {
            $this->throwException('Inactive user', 403);
        }
        if ($user->getSuspended()) {
            $this->throwException('Suspended Account', 403);
        }

        if ($user->getRememberToken()) {
            $this->throwException('Awaiting passsword reset', 403);
        }
    }

    /**
     * Logs a user into the system and adds the user's info into the session.
     * Fires user:login event.
     * @param string $username
     * @param string $password
     */
    public function login($username, $password, $usernameAndEmail = true)
    {
        // Check if user is already logged in
        if (Session::has('user_data')) {
            $this->throwException(
                trans('errors.http_401'),
                403
            );
        }

        // Get the user based on the username
        /** @var User $user */
        $user = $this->userRepo->getByUsernameOrEmail($username);

        if (!isset($user)) {
            $this->throwException('User not found', 404);
        }

        $hashedInput = hash('sha512', $password);
        if ($hashedInput != $user->getPassword()) {
            $this->throwException('Password or username invalid', 403);
        }

        // Is user valid?
        $this->checkUserValid($user);

        // User can login now
        $userData = [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName()
        ];

        // Check to see if there are any leagues associated with the user
        // and mark the user's data for access.

        $leagues = $this->leagueRepo->getUserLeagues($user->getId());

        $userData['league_admin'] = count($leagues) > 0;

        Session::set('user_data', $userData);

        Auth::login($user);

        // Fire Event -->
        Event::fire(new UserLoginEvent($user, true));
    }

    /**
     * Logs a user out of the system and clears data for that user.
     * Fires user:logout event.
     * @throws ServiceException
     */
    public function logout()
    {
        if (!Session::has('user_data')) {
            $this->throwException("Not logged in", 403);
        }

        /** @var User $user */
        $userData = Session::get('user_data');
        $user = $this->userRepo->get($userData['id']);

        if (!$user instanceof User) {
            $this->throwException(trans('errors.http_401'), 403);
        }

        // Clear the session
        Session::forget('user_data');

        // Fire Event -->
        Event::fire(new UserLoginEvent($user, false));
    }

    /**
     * Resets the users password and generates a new token for that resetting of that password.
     * Fires user:password:reset event
     * @param integer $email
     */
    public function resetPassword($email, $suppressEvent = false)
    {
        $newpass = substr(hash('sha256', mt_rand()), 0, 15);

        // Get the user based on the username
        /** @var User $user */
        $user = $this->userRepo->getBy([
            "email" => $email
        ]);

        if (!isset($user)) {
            $this->throwException(trans('errors.http_404_object', ['object' => 'User']), 404);
        }

        // If there is existing reset request, then delete it
        $existingReset = $this->passwordResetRepo->getBy([
            'userId' => $user->getId()
        ]);

        if ($existingReset instanceof PasswordReset) {
            $this->passwordResetRepo->remove($existingReset);
        }

        // Save a random password to the user
        $user->setPassword($newpass);
        EM::persist($user);

        $tokenStr = substr(hash('sha256', mt_rand()), 0, 30);
        $date = new \DateTime();

        $reset = new PasswordReset($tokenStr);
        $reset->setExpiry($date->modify('+3 day'));
        $reset->setUserId($email);

        EM::flush();

        // Fire Event -->
        if (!$suppressEvent) {
            Event::fire(new PasswordResetEvent($user));
        }
    }

    /**
     * Change a user's password temporarily to random string and create password reset for the user.
     * Fires user:password:changed event.
     * @throws ServiceException
     */
    public function changePassword()
    {
        $token = Input::get('token');
        $newPassword = Input::get('password');
        $username = Input::get('username');

        /** @var PasswordReset $reset */
        $reset = $this->passwordResetRepo->getBy([
            'token' => $token
        ]);

        if (!$reset) {
            $this->throwException("Change password token not found", 404);
        }

        // Ensure not expired
        $now = new \DateTime();
        if ($now >= $reset->getExpiry()) {
            $this->throwException("Password change token is expired.", 403);
        }

        $user = $this->userRepo->getByUsernameOrEmail($username);
        if (!$user) {
            $this->throwException("Password or username invalid: $username", 404);
        }

        $hashedInput = hash('sha512', $newPassword);
        $user->setPassword($hashedInput);

        EM::persist($user);

        EM::remove($reset);

        EM::flush();

        // Fire Event -->
        Event::fire(new PasswordResetEvent($user));
    }

    /**
     * Registers a user with the system.
     * Fires user:registration event.
     * @param string $email
     * @param string $username
     */
    public function register($email, $username)
    {
        // Check to see if the email address is already registered
        $user = $this->userRepo->getByEmail($email);
        if (isset($user)) {
            Log::error("Username, $username is already in use");
            $this->throwException("Email is already registered", 403);
        }

        // Check if the username is already in use.
        $user = $this->userRepo->getByUsername($username);
        if ($user) {
            Log::error(json_encode($user));
            Log::error("Username, $username is already in use");
            $this->throwException("Username unavailable", 403);
        }

        //--------------------------
        // Create user and add them to players group
        //--------------------------

        $group = $this->groupRepo->getBy([
            'name' => 'player'
        ]);

        if (!$group) {
            $this->throwException("System problem", 500);
        }

        $user = new User();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setGroup($group);
        $user->setStatus(User::STATUS_REGISTERED_NOT_VERIFIED);
        EM::persist($user);
        EM::flush();

        $reg = new Registration();
        $reg->setToken(
            substr(
                hash('sha512', $user->getEmail() . time()),
                0,
                63
            )
        );
        $reg->setExpiry(new \Datetime('+7 days'));
        $reg->setUser($user);
        EM::persist($reg);
        EM::flush();

        // Reset the user's password.
        $this->resetPassword($user->getEmail(), true);

        // Fire Event -->
        Event::fire(new UserRegistrationEvent($user, $reg->getToken()));
    }

    /**
     * Confirms a registration for a user.
     * Fires user:confirmation event
     * @param string $token Token used to identify registration
     * @throws ServiceException
     */
    public function confirmRegistration($token)
    {
        $reg = $this->regRepo->getBy([
            'token' => $token
        ]);

        if (!$reg instanceof Registration) {
            $this->throwException(
                trans('errors.http_404_object', ['object' => 'Registration']),
                403
            );
        }

        if ($reg->getExpiry() < new \DateTime()) {
            $this->throwException(
                trans('errors.http_403_registration_expired'),
                403
            );
        }

        $user = $reg->getUser();
        $user->setStatus(User::STATUS_VERIFIED);
        EM::persist($user);

        EM::remove($reg);

        EM::flush();

        Event::fire(new UserConfirmationEvent($user));
    }

    /** Retrieve a user based on id. */
    public function getUser($userId)
    {
        return $this->userRepo->get($userId);
    }


}
