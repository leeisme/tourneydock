<?php

namespace Fudave\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Other
        App::bind('Leebo\Acl\AclProviderInterface', 'Leebo\Acl\DoctrineAclProvider');
    }
}
