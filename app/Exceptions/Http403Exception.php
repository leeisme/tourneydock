<?php namespace Tourney\Exceptions;

use Exception;

class Http403Exception extends ApplicationException
{
    public function __construct($message = '', $code = 403, Exception $previous = null) {
        if ($message == '') {
            $message = \Lang::get('errors.http_403');
        }
        parent::__construct($message, $code, $previous);
    }
}