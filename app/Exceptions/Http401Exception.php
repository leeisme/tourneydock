<?php namespace Tourney\Exceptions;

use Exception;

class Http401Exception extends ApplicationException
{
    public function __construct($message = '', $code = 401, Exception $previous = null) {
        if ($message == '') {
            $message = \Lang::get('errors.http_401');
        }
        parent::__construct($message, $code, $previous);
    }
}