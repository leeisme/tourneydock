<?php namespace Tourney\Exceptions;

use Exception;

class Http400Exception extends ApplicationException
{
    public function __construct($message = '', $code = 400, Exception $previous = null) {
        if ($message == '') {
            $message = \Lang::get('errors.http_400');
        }
        parent::__construct($message, $code, $previous);
    }
}