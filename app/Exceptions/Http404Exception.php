<?php namespace Tourney\Exceptions;

use Exception;

class Http404Exception extends ApplicationException
{
    public function __construct($message = '', $code = 404, Exception $previous = null) {
        if ($message == '') {
            $message = \Lang::get('errors.http_404');
        }
        parent::__construct($message, $code, $previous);
    }
}