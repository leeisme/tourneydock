<?php namespace Tourney\Exceptions;

use Exception;

class Http500Exception extends ApplicationException
{
    public function __construct($message = '', $code = 500, Exception $previous = null) {
        if ($message == '') {
            $message = \Lang::get('errors.http_500');
        }
        parent::__construct($message, $code, $previous);
    }
}