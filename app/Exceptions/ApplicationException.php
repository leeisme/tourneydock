<?php
namespace Tourney\Exceptions;

use \Exception;

/**
 * Basic Exception classes for model related errors.
 */
class ApplicationException extends Exception {

}