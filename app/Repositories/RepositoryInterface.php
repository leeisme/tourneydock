<?php

namespace Repositories;

interface RepositoryInterface
{
    /**
     * Basic get function to return an object by id
     * @param integer $id
     * @param array $with
     * @param bool $asArray
     * @return mixed
     */
    public function get($id, array $with = [], $asArray = false);

    /**
     * @param array $filterPairs Associative array specifying the name/value pairs to base where on.
     * @param array $with
     * @param bool $asArray
     * @return mixed
     */
    public function getBy(array $filterPairs, array $with = [], $asArray = false);

    /**
     * Returns an array of objects matching the $filterPairs argument
     * @param array $filterPairs
     * @param array $orderBy
     * @param array $with
     * @param bool $asArray
     * @return array
     */
    public function getListBy(array $filterPairs = [], array $orderBy = [], array $with = [], $asArray = false);

    /**
     * Same as getListBy but introduces pagination functionality
     * @param int $start
     * @param int $limit
     * @param array $filterPairs
     * @param array $orderBy
     * @param array $with
     * @param bool $asArray
     * @return array
     */
    public function paginateBy(
        $start = -1,
        $limit = 10,
        array $filterPairs = [],
        array $orderBy = [],
        array $with = [],
        $asArray = false
    );

    /**
     * Saves an object.
     * @param object $entity
     * @return mixed
     */
    public function save($entity);

    /**
     * Delete a model/entity
     * @param $entity
     * @return mixed
     */
    public function remove($entity);

    /**
     * Delete entities using matching pairs
     * @param array $filterPairs
     * @return int count of entities removed.
     */
    public function removeBy(array $filterPairs = []);

}