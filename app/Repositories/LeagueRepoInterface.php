<?php

namespace Repositories;

use Entity\User;

interface LeagueRepoInterface extends RepositoryInterface
{

    /**
     * Check to see if a user own a league.
     * @param $user_id
     * @return bool
     */
    public function userHasLeagues($user_id);

    /** Returns an array of League objects for which a User is subscribed to. */
    public function getUserLeagues($user_id);

    /**
     * Check to see if a User is already a member of a league
     * @param int $userId
     * @param $leagueId
     * @return User
     */
    public function getUserLeagueMembership($userId, $leagueId);
}