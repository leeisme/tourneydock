<?php

namespace Repositories\Doctrine;

use EM;
use Log;
use Entity\User;
use Repositories\UserRepoInterface;

class UserDoctrineRepository extends AbstractDoctrineRepository implements UserRepoInterface
{

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    protected function getFQCN()
    {
        return User::class;
    }

    /**
     * @param string $needle Username or password to locate the user by.
     * @return User
     * @throws \Exception
     */
    public function getByUsernameOrEmail($needle)
    {
        $qb = em::createQueryBuilder()
            ->select('o')
            ->from('Entity\User', 'o')
            ->where('o.username = :needle')
            ->orWhere('o.email = :needle')
            ->setParameter('needle', $needle);

        try {
            $qry = $qb->getQuery();
            $user = $qry->getOneOrNullResult();
            return $user;
        } catch(\Exception $e) {
            Log::error("Exception: {$e->getMessage()}");
            throw $e;
        }

    }

    /**
     * Return a User using the email address for lookup.
     * @param string $email
     * @return User
     */
    public function getByEmail($email)
    {
        $qb = em::createQueryBuilder()
            ->select('o')
            ->from('Entity\User', 'o')
            ->where('o.email = :email')
            ->setParameter('email', $email);

        $user = $qb->getQuery()->getOneOrNullResult();

        return $user;
    }

    /**
     * Return a User using the username for lookup.
     * @param string $username
     * @return User
     */
    public function getByUsername($username)
    {
        return $this->getBy([
            'username' => $username
        ]);
    }

    /**
     * Retrieve an array of Users who are followers of the passed in user. Allow
     * paging if $start and $limit parameters are supplied.
     * @param int $userId
     * @param int $start
     * @param int $limit
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getFollowers($userId, $start = -1, $limit = -1)
    {
        $sql = "
            SELECT
              u.id,
              u.username,
              concat(u.first_name, ' ', u.last_name) as fullName
            FROM
              users u
              INNER JOIN followers f on f.follower_id = u.id
            WHERE
              f.followed_id = :followedId
            ORDER BY
              u.username
        ";

        if ($start >= 1 && $limit >= 1) {
            $sql .= "LIMIT {$limit} OFFSET {$start}";
        }

        $stmt = em::getConnection()->prepare($sql);

        $stmt->execute([
            "followedId" => $userId
        ]);

        $result = $stmt->fetchAll();

        return $result;
    }

    /**
     * Return an array of User objects that the passed in user is following.
     * @param int $userId
     * @param int $start
     * @param int $limit
     * @return array
     */
    public function getFollowed($userId, $start = -1, $limit = -1)
    {
        $sql = "
            SELECT
              f.followed_id,
              f.follower_id,
              u.id,
              u.username,
              concat(u.first_name, ' ', u.last_name) as full_name
            FROM
              users u
              INNER JOIN followers f on f.followed_id = u.id
            WHERE
              f.follower_id = :followerId
            ORDER BY
              u.username;
        ";

        if ($start >= 1 && $limit >= 1) {
            $sql .= "LIMIT {$limit} OFFSET {$start}";
        }

        $stmt = em::getConnection()->prepare($sql);

        $stmt->execute([
            "followerId" => $userId
        ]);

        $result = $stmt->fetchAll();

        return $result;
    }
}