<?php

namespace Fudave\Repositories\Doctrine;

use Entity\PasswordReset;
use Repositories\PasswordResetRepoInterface;

class PasswordResetDoctrineRepo extends AbstractDoctrineRepository implements PasswordResetRepoInterface
{

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    protected function getFQCN()
    {
        return PasswordReset::class;
    }
}