<?php

use Entity\Acl;

class AclDoctrineRepository extends \Fudave\Repositories\Doctrine\AbstractDoctrineRepository
{

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    protected function getFQCN()
    {
        return Acl::class;
    }

}