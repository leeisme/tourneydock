<?php

namespace Repositories\Doctrine;

use EM;
use Log;
use Repositories\RepositoryInterface;

abstract class AbstractDoctrineRepository implements RepositoryInterface
{

    private $aliases = ['a','b', 'c', 'd', 'e', 'f', 'g', 'h'];

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    abstract protected function getFQCN();

    /**
     * Basic get function to return an object by id
     * @param integer $id
     * @param array $with
     * @param bool $asArray
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get($id, array $with = [], $asArray = false)
    {
        // Select portion
        $selectArray = ['z'];
        for($i = 0; $i < count($with); $i++) {
            $selectArray[] = $this->aliases[$i];
        }

        $qb = $this->createQueryBuilder(['id' => $id], [], $with)
            ->where('z.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filterPairs
     * @return mixed
     */
    public function getBy(array $filterPairs, array $with = [], $asArray = false)
    {

        $qb = $this->createQueryBuilder($filterPairs, [], $with);

        if (!$asArray) {
            $list = $qb->getQuery()->getResult();
            if (count($list) > 0) {
                return $list[0];
            }

            return null;

        } else {
            $result = $qb->getQuery()->getArrayResult();
            if (count($result)) {
                return $result[0];
            }
        }

        return null;
    }

    /**
     * Builds a basic query for retrieving a list.
     * @param array $filterPairs
     * @param array $orderBy
     * @param array $with
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilder(
        array $filterPairs = [],
        array $orderBy = [],
        array $with = []
    ) {

        // Select portion
        $selectArray = ['z'];
        for($i = 0; $i < count($with); $i++) {
            $selectArray[] = $this->aliases[$i];
        }

        // Create basic QueryBuilder
        $qb = em::createQueryBuilder()
            ->select($selectArray)
            ->from($this->getFQCN(), 'z');

        // If $with is populated, we need to join the association
        if (count($with) > 0) {

            for($i = 0; $i < count($with); $i++) {
                $assoc = 'z.' . $with[$i];
                $qb->join($assoc, $this->aliases[$i]);
            }
        }

        // Filtering
        foreach ($filterPairs as $name => $value) {

            $qb->andWhere("z.{$name} = :{$name}");
            $qb->setParameter($name, $value);
        }

        // Order By
        foreach ($orderBy as $name => $direction) {
            $qb->addOrderBy($name, $direction);
        }

        return $qb;
    }

    public function getListBy(
        array $filterPairs = [],
        array $orderBy = [],
        array $with = [],
        $asArray = false
    ) {

        $qb = $this->createQueryBuilder($filterPairs, $orderBy);

        if (!$asArray) {
            return $qb->getQuery()->getResult();
        } else {
            return $qb->getQuery()->getArrayResult();
        }
    }

    /**
     * Save an object.
     * @param object $object
     * @return void
     */
    public function save($object)
    {
        em::persist($object);
        em::flush();
    }

    /**
     * Same as getListBy but introduces pagination functionality
     * @param int $start Starting record to retrieve (Starting at 1)
     * @param int $limit Max number of records to retrieve (IE: page size)
     * @param array $filterPairs Array of filter pairs
     * @param array $orderBy
     * @param bool $asArray
     * @return mixed
     */
    public function paginateBy(
        $start = -1,
        $limit = 10,
        array $filterPairs = [],
        array $orderBy = [],
        array $with = [],
        $asArray = false)
    {
        $qb = $this->createQueryBuilder($filterPairs, $orderBy);

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($qb->getQuery());
        $paginator
            ->getQuery()
            ->setFirstResult($start - 1)
            ->setMaxResults($limit);

        if ($asArray) {
            return $paginator->getQuery()->getArrayResult();
        } else {
            return $paginator->getQuery()->getResult();
        }
    }

    /**
     * Delete a model/entity
     * @param $entity
     * @return mixed
     */
    public function remove($entity)
    {
        em::remove($entity);
        em::flush();
    }

    /**
     * Delete entities using matching pairs
     * @param array $filterPairs
     * @return int count of entities removed.
     */
    public function removeBy(array $filterPairs = [])
    {

        $qb = em::createQueryBuilder()
            ->delete($this->getFQCN(), 'o');

        // Filtering
        foreach ($filterPairs as $name => $value) {

            $qb->andWhere("o.{$name} = :{$name}");
            $qb->setParameter($name, $value);
        }

        $qb->getQuery()->execute();
    }
}