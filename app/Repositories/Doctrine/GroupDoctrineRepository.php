<?php

namespace Tourney\Repositories\Doctrine;

use Entity\Group;
use Repositories\Doctrine\AbstractDoctrineRepository;
use Repositories\GroupRepoInterface;

class GroupDoctrineRepository extends AbstractDoctrineRepository implements GroupRepoInterface
{


	public function __construct(
		Group $model
	)
	{
		$this->model = $model;
	}

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    protected function getFQCN()
    {
        return Group::class;
    }
}