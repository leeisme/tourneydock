<?php

namespace Tourney\Repositories\Doctrine;


use EM;
use Entity\League;
use Entity\User;
use Repositories\LeagueRepoInterface;

class LeagueDoctrineRepository extends AbstractDoctrineRepository implements LeagueRepoInterface
{

    /**
     * Force descendant classes to provide a FQCN
     * @return string
     */
    protected function getFQCN()
    {
        return League::class;
    }

    /** Returns an array of League objects for which a User is subscribed to. */
    public function getUserLeagues($user_id)
    {
        $qb = EM::createQuery("
            SELECT l
            FROM
              Entity\\League l
            JOIN l.user u
            WHERE
              u.id = :id
        ");

        $qb->setParameter('id', $user_id);

        $leagues = $qb->getResult();

        return $leagues;
    }

    /** Check to see if a User is already a member of a league
     * @param int $userId
     * @param int $leagueId
     * @return User
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserLeagueMembership($userId, $leagueId)
    {
        $q = EM::createQuery("
            SELECT l FROM Entity\\League l
            JOIN l.user u
            WHERE
              u.id = :user_id
              AND
              l.id = :league_id
        ");
        $q->setParameter('user_id', $userId);
        $q->setParameter('league_id', $leagueId);

        $existingLeague = $q->getOneOrNullResult();

        return $existingLeague;
    }

    /**
     * Check to see if a user own a league.
     * @param $user_id
     * @return bool
     */
    public function userHasLeagues($user_id)
    {
        $q = EM::createQuery("
            SELECT FIRST l FROM Entity\\League l
            JOIN l.user u
            WHERE
              u.id = :user_id
              AND
              l.id = :league_id
        ");
        $q->setParameter('user_id', $userId);
        $q->setParameter('league_id', $leagueId);

        $existingLeague = $q->getOneOrNullResult();

        return isset($existingLeague);
    }
}