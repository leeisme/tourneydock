<?php

namespace Repositories;

use Entity\User;

/**
 * Interface UserRepoInterface
 * @package Fudave\Repositories
 */
interface UserRepoInterface extends RepositoryInterface
{
    /**
     * @param string $needle Username or password to locate the user by.
     * @return User
     */
    public function getByUsernameOrEmail($needle);

    /**
     * Return a User using the email address for lookup.
     * @param string $email
     * @return User
     */
    public function getByEmail($email);

    /**
     * Return a User using the username for lookup.
     * @param string $username
     * @return User
     */
    public function getByUsername($username);

    /**
     * Retrieve an array of Users were are followers of the passed in user
     * @param int $userId
     * @return array
     */
    public function getFollowers($userId, $start = -1, $limit = -1);

    /**
     * Return an array of User objects that the passed in user is following.
     * @param int $userId
     * @param int $start
     * @param int $limit
     * @return array
     */
    public function getFollowed($userId, $start = -1, $limit = -1);
}