<?php

namespace Entity;

/**
 * Buyin
 */
class Buyin
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var integer
     */
    private $startLevel;

    /**
     * @var integer
     */
    private $stopLevel;

    /**
     * @var \Entity\Tourney
     */
    private $tourney;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Buyin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Buyin
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Buyin
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set startLevel
     *
     * @param integer $startLevel
     *
     * @return Buyin
     */
    public function setStartLevel($startLevel)
    {
        $this->startLevel = $startLevel;

        return $this;
    }

    /**
     * Get startLevel
     *
     * @return integer
     */
    public function getStartLevel()
    {
        return $this->startLevel;
    }

    /**
     * Set stopLevel
     *
     * @param integer $stopLevel
     *
     * @return Buyin
     */
    public function setStopLevel($stopLevel)
    {
        $this->stopLevel = $stopLevel;

        return $this;
    }

    /**
     * Get stopLevel
     *
     * @return integer
     */
    public function getStopLevel()
    {
        return $this->stopLevel;
    }

    /**
     * Set tourney
     *
     * @param \Entity\Tourney $tourney
     *
     * @return Buyin
     */
    public function setTourney(\Entity\Tourney $tourney = null)
    {
        $this->tourney = $tourney;

        return $this;
    }

    /**
     * Get tourney
     *
     * @return \Entity\Tourney
     */
    public function getTourney()
    {
        return $this->tourney;
    }
}
