<?php

namespace Entity;

/**
 * League
 */
class League
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $visibility;

    /**
     * @var integer
     */
    private $access;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $schedules;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tourneys;

    /**
     * @var \Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $players;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schedules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tourneys = new \Doctrine\Common\Collections\ArrayCollection();
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return League
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     *
     * @return League
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set access
     *
     * @param integer $access
     *
     * @return League
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return integer
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return League
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Add schedule
     *
     * @param \Entity\TourneySchedule $schedule
     *
     * @return League
     */
    public function addSchedule(\Entity\TourneySchedule $schedule)
    {
        $this->schedules[] = $schedule;

        return $this;
    }

    /**
     * Remove schedule
     *
     * @param \Entity\TourneySchedule $schedule
     */
    public function removeSchedule(\Entity\TourneySchedule $schedule)
    {
        $this->schedules->removeElement($schedule);
    }

    /**
     * Get schedules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }

    /**
     * Add tourney
     *
     * @param \Entity\Tourney $tourney
     *
     * @return League
     */
    public function addTourney(\Entity\Tourney $tourney)
    {
        $this->tourneys[] = $tourney;

        return $this;
    }

    /**
     * Remove tourney
     *
     * @param \Entity\Tourney $tourney
     */
    public function removeTourney(\Entity\Tourney $tourney)
    {
        $this->tourneys->removeElement($tourney);
    }

    /**
     * Get tourneys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTourneys()
    {
        return $this->tourneys;
    }

    /**
     * Set user
     *
     * @param \Entity\User $user
     *
     * @return League
     */
    public function setUser(\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add player
     *
     * @param \Entity\User $player
     *
     * @return League
     */
    public function addPlayer(\Entity\User $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \Entity\User $player
     */
    public function removePlayer(\Entity\User $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
}
