<?php

namespace Entity;

/**
 * Tourney
 */
class Tourney
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $startTime;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var boolean
     */
    private $isTemplate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $entries;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $buyins;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $prizes;

    /**
     * @var \Entity\Campaign
     */
    private $campaign;

    /**
     * @var \Entity\Tourney
     */
    private $parent;

    /**
     * @var \Entity\League
     */
    private $league;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->buyins = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prizes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tourney
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tourney
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Tourney
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Tourney
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     *
     * @return Tourney
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }

    /**
     * Add child
     *
     * @param \Entity\Tourney $child
     *
     * @return Tourney
     */
    public function addChild(\Entity\Tourney $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Entity\Tourney $child
     */
    public function removeChild(\Entity\Tourney $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add entry
     *
     * @param \Entity\Entry $entry
     *
     * @return Tourney
     */
    public function addEntry(\Entity\Entry $entry)
    {
        $this->entries[] = $entry;

        return $this;
    }

    /**
     * Remove entry
     *
     * @param \Entity\Entry $entry
     */
    public function removeEntry(\Entity\Entry $entry)
    {
        $this->entries->removeElement($entry);
    }

    /**
     * Get entries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntries()
    {
        return $this->entries;
    }

    /**
     * Add buyin
     *
     * @param \Entity\Buyin $buyin
     *
     * @return Tourney
     */
    public function addBuyin(\Entity\Buyin $buyin)
    {
        $this->buyins[] = $buyin;

        return $this;
    }

    /**
     * Remove buyin
     *
     * @param \Entity\Buyin $buyin
     */
    public function removeBuyin(\Entity\Buyin $buyin)
    {
        $this->buyins->removeElement($buyin);
    }

    /**
     * Get buyins
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBuyins()
    {
        return $this->buyins;
    }

    /**
     * Add prize
     *
     * @param \Entity\Prize $prize
     *
     * @return Tourney
     */
    public function addPrize(\Entity\Prize $prize)
    {
        $this->prizes[] = $prize;

        return $this;
    }

    /**
     * Remove prize
     *
     * @param \Entity\Prize $prize
     */
    public function removePrize(\Entity\Prize $prize)
    {
        $this->prizes->removeElement($prize);
    }

    /**
     * Get prizes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrizes()
    {
        return $this->prizes;
    }

    /**
     * Set campaign
     *
     * @param \Entity\Campaign $campaign
     *
     * @return Tourney
     */
    public function setCampaign(\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set parent
     *
     * @param \Entity\Tourney $parent
     *
     * @return Tourney
     */
    public function setParent(\Entity\Tourney $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Entity\Tourney
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set league
     *
     * @param \Entity\League $league
     *
     * @return Tourney
     */
    public function setLeague(\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }
}
