<?php

namespace Entity;

/**
 * Entry
 */
class Entry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var integer
     */
    private $rank;

    /**
     * @var boolean
     */
    private $placed;

    /**
     * @var \Entity\Tourney
     */
    private $tourney;

    /**
     * @var \Entity\User
     */
    private $player;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Entry
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Entry
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set placed
     *
     * @param boolean $placed
     *
     * @return Entry
     */
    public function setPlaced($placed)
    {
        $this->placed = $placed;

        return $this;
    }

    /**
     * Get placed
     *
     * @return boolean
     */
    public function getPlaced()
    {
        return $this->placed;
    }

    /**
     * Set tourney
     *
     * @param \Entity\Tourney $tourney
     *
     * @return Entry
     */
    public function setTourney(\Entity\Tourney $tourney = null)
    {
        $this->tourney = $tourney;

        return $this;
    }

    /**
     * Get tourney
     *
     * @return \Entity\Tourney
     */
    public function getTourney()
    {
        return $this->tourney;
    }

    /**
     * Set player
     *
     * @param \Entity\User $player
     *
     * @return Entry
     */
    public function setPlayer(\Entity\User $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Entity\User
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
