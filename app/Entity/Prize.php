<?php

namespace Entity;

/**
 * Prize
 */
class Prize
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var integer
     */
    private $rank;

    /**
     * @var \Entity\Tourney
     */
    private $tourney;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Prize
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Prize
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Prize
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Prize
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set tourney
     *
     * @param \Entity\Tourney $tourney
     *
     * @return Prize
     */
    public function setTourney(\Entity\Tourney $tourney = null)
    {
        $this->tourney = $tourney;

        return $this;
    }

    /**
     * Get tourney
     *
     * @return \Entity\Tourney
     */
    public function getTourney()
    {
        return $this->tourney;
    }
}
