<?php

namespace Entity;

/**
 * TourneySchedule
 */
class TourneySchedule
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $length;

    /**
     * @var integer
     */
    private $numLevels;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $levels;

    /**
     * @var \Entity\League
     */
    private $league;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->levels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TourneySchedule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TourneySchedule
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return TourneySchedule
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set length
     *
     * @param integer $length
     *
     * @return TourneySchedule
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set numLevels
     *
     * @param integer $numLevels
     *
     * @return TourneySchedule
     */
    public function setNumLevels($numLevels)
    {
        $this->numLevels = $numLevels;

        return $this;
    }

    /**
     * Get numLevels
     *
     * @return integer
     */
    public function getNumLevels()
    {
        return $this->numLevels;
    }

    /**
     * Add level
     *
     * @param \Entity\BlindLevel $level
     *
     * @return TourneySchedule
     */
    public function addLevel(\Entity\BlindLevel $level)
    {
        $this->levels[] = $level;

        return $this;
    }

    /**
     * Remove level
     *
     * @param \Entity\BlindLevel $level
     */
    public function removeLevel(\Entity\BlindLevel $level)
    {
        $this->levels->removeElement($level);
    }

    /**
     * Get levels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * Set league
     *
     * @param \Entity\League $league
     *
     * @return TourneySchedule
     */
    public function setLeague(\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }
}
