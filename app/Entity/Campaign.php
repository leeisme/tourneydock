<?php

namespace Entity;

/**
 * Campaign
 */
class Campaign
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $leagueId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tourneys;

    /**
     * @var \Entity\League
     */
    private $league;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $players;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tourneys = new \Doctrine\Common\Collections\ArrayCollection();
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Campaign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Campaign
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Campaign
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Campaign
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set leagueId
     *
     * @param integer $leagueId
     *
     * @return Campaign
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;

        return $this;
    }

    /**
     * Get leagueId
     *
     * @return integer
     */
    public function getLeagueId()
    {
        return $this->leagueId;
    }

    /**
     * Add tourney
     *
     * @param \Entity\Tourney $tourney
     *
     * @return Campaign
     */
    public function addTourney(\Entity\Tourney $tourney)
    {
        $this->tourneys[] = $tourney;

        return $this;
    }

    /**
     * Remove tourney
     *
     * @param \Entity\Tourney $tourney
     */
    public function removeTourney(\Entity\Tourney $tourney)
    {
        $this->tourneys->removeElement($tourney);
    }

    /**
     * Get tourneys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTourneys()
    {
        return $this->tourneys;
    }

    /**
     * Set league
     *
     * @param \Entity\League $league
     *
     * @return Campaign
     */
    public function setLeague(\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Add player
     *
     * @param \Entity\User $player
     *
     * @return Campaign
     */
    public function addPlayer(\Entity\User $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \Entity\User $player
     */
    public function removePlayer(\Entity\User $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
}
