<?php

namespace Entity;

/**
 * BlindLevel
 */
class BlindLevel
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $levelType;

    /**
     * @var integer
     */
    private $length;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var string
     */
    private $ante;

    /**
     * @var string
     */
    private $smallBlind;

    /**
     * @var string
     */
    private $bigBlind;

    /**
     * @var boolean
     */
    private $chipUp;

    /**
     * @var \Entity\TourneySchedule
     */
    private $schedule;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BlindLevel
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set levelType
     *
     * @param integer $levelType
     *
     * @return BlindLevel
     */
    public function setLevelType($levelType)
    {
        $this->levelType = $levelType;

        return $this;
    }

    /**
     * Get levelType
     *
     * @return integer
     */
    public function getLevelType()
    {
        return $this->levelType;
    }

    /**
     * Set length
     *
     * @param integer $length
     *
     * @return BlindLevel
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return BlindLevel
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set ante
     *
     * @param string $ante
     *
     * @return BlindLevel
     */
    public function setAnte($ante)
    {
        $this->ante = $ante;

        return $this;
    }

    /**
     * Get ante
     *
     * @return string
     */
    public function getAnte()
    {
        return $this->ante;
    }

    /**
     * Set smallBlind
     *
     * @param string $smallBlind
     *
     * @return BlindLevel
     */
    public function setSmallBlind($smallBlind)
    {
        $this->smallBlind = $smallBlind;

        return $this;
    }

    /**
     * Get smallBlind
     *
     * @return string
     */
    public function getSmallBlind()
    {
        return $this->smallBlind;
    }

    /**
     * Set bigBlind
     *
     * @param string $bigBlind
     *
     * @return BlindLevel
     */
    public function setBigBlind($bigBlind)
    {
        $this->bigBlind = $bigBlind;

        return $this;
    }

    /**
     * Get bigBlind
     *
     * @return string
     */
    public function getBigBlind()
    {
        return $this->bigBlind;
    }

    /**
     * Set chipUp
     *
     * @param boolean $chipUp
     *
     * @return BlindLevel
     */
    public function setChipUp($chipUp)
    {
        $this->chipUp = $chipUp;

        return $this;
    }

    /**
     * Get chipUp
     *
     * @return boolean
     */
    public function getChipUp()
    {
        return $this->chipUp;
    }

    /**
     * Set schedule
     *
     * @param \Entity\TourneySchedule $schedule
     *
     * @return BlindLevel
     */
    public function setSchedule(\Entity\TourneySchedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \Entity\TourneySchedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }
}
