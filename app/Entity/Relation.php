<?php

namespace Entity;

/**
 * Relation
 */
class Relation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \Entity\User
     */
    private $followed;

    /**
     * @var \Entity\User
     */
    private $follower;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Relation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Relation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set followed
     *
     * @param \Entity\User $followed
     *
     * @return Relation
     */
    public function setFollowed(\Entity\User $followed = null)
    {
        $this->followed = $followed;

        return $this;
    }

    /**
     * Get followed
     *
     * @return \Entity\User
     */
    public function getFollowed()
    {
        return $this->followed;
    }

    /**
     * Set follower
     *
     * @param \Entity\User $follower
     *
     * @return Relation
     */
    public function setFollower(\Entity\User $follower = null)
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * Get follower
     *
     * @return \Entity\User
     */
    public function getFollower()
    {
        return $this->follower;
    }
}
