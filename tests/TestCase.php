<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Creates a url and optionally adds the XDEBUG_SESSION_START=key
     * @param string $str
     * @return string
     */
    protected function makeUrl($str)
    {
        $separater = (strpos($str, '?') ? '&' : '?');
        $result = $str . $separater . 'XDEBUG_SESSION_START=homestead';

        return $result;
    }
}
