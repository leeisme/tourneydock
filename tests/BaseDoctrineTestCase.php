<?php

namespace Tests;

use Auth;
use Entity\User;

class BaseDoctrineTestCase extends TestCase
{
    /**
     * @ignore
     */
    public function setup()
    {
        parent::setUp();

        $user = new User();
        $user->setFirstName('test');
        $user->setLastName('user');
        Auth::login($user);

        $this->session(array(
            'user_data' => array(
                'id' => 1,
                'firstName' => 'Sean',
                'lastName' => 'Backer'
            )
        ));
    }
}