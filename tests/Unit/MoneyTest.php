<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Money\Money;
use Money\Currency;

class MoneyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUSAdd()
    {
        $five = new Money(500, new Currency('USD'));
        $fiftyCent = new Money(50, new Currency('USD'));

        $total = $five->add($fiftyCent);

        echo 'Total: ' . $total->getAmount() . PHP_EOL;

        $more = new Money(150, new Currency('USD'));

        $newTotal = $total->add($more);

        echo 'Currency: ' . $newTotal->getAmount() . PHP_EOL;

        $this->assertTrue($newTotal->equals(new Money(700, new Currency('USD'))));

    }

}
