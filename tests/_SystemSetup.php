<?php

use Tests\TestCase;

class _SystemSetup extends TestCase
{
    /**
     * @test
     */
    public function it_should_setup_database()
    {
        Artisan::call('doctrine:migrations:refresh');
        Artisan::call('db:seed');
        $this->session(array(
            'league_id' => 1,
            'user_id' => 1
        ));
    }
}