<?php

namespace Test;

use Tests\BaseDoctrineTestCase;

class IdentityServiceTest extends BaseDoctrineTestCase
{

    /**
     * Test registering a user.
     * @test
     * @group identity
     * @group main
     */
    public function it_registers_a_user()
    {
        $params = array(
            'username' => 'rockemsockem',
            'email' => 'rockemsockem@somedomain.com'
        );

        $response = $this->call('POST', $this->makeUrl('api/v1/identity/register'), $params);
        $result = $response->isOk();
        if (!$result) {
            var_dump($response->getContent());
            \Log::info($response->getContent());
        }

        $this->assertTrue($result);
    }

    /**
     * @test
     * @group identity
     */
    public function it_confirms_user_registration()
    {

        $this->json('GET', $this->makeUrl('identity/confirm/1234'));

        $result = $this->response->status() == 200;

        if (!$result) {
            var_dump($this->response->getContent());
            \Log::info($this->response->getContent());
        }

        $this->assertTrue($result);
    }

    /**
     * Test resetting the users password.
     * @test
     * @group identity
     */
    public function it_resets_the_users_password()
    {
        $params = array(
            'email' => 'admin@nodomain.com'
        );

        $this->json('POST', $this->makeUrl('api/identity/reset'), $params);
        $result = $this->response->status() == 200;

        $content = $this->response->getContent();

        if (!$result) {
            var_dump($content);
            \Log::info($content);
        }

        $this->assertTrue($result);
    }

    /**
     * Test Changing a password given a token.
     * @test
     * @group identity
     * @group main
     */
    public function it_changes_the_password()
    {
        $params = array(
            'username' => 'plainuser@none.com',
            'token' => '1234',
            'password' => '1234'
        );

        $this->json('POST', $this->makeUrl('api/v1/identity/changepassword'), $params);

        $result = $this->response->status() == 200;

        if (!$result) {
            var_dump($this->response->getContent());
            \Log::info($this->response->getContent());
        }

        $this->assertTrue($result);
    }

    /**
     * Test Logging the user in.
     * @test
     * @group identity
     * @group main
     */
    public function it_logs_the_newly_created_user_in()
    {
        // For this particular test, we need to make sure that login
        // information is not present.
        Session::flush();

        $params = array(
            'email' => 'plainuser@none.com',
            'password' => '1234'
        );

        $this->json('POST', $this->makeUrl('api/v1/identity/login'), $params);
        $result = $this->response->status() == 200;

        if (!$result) {
            var_dump($this->response->getContent());
            \Log::info($this->response->getContent());
        }

        $this->assertTrue($result);
    }

}